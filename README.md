# TipTop Pay модуль для PrestaShop

Модуль позволит добавить на ваш сайт оплату банковскими картами через платежный сервис [TipTop Pay](https://tiptoppay.kz). 
Для корректной работы модуля необходима регистрация в сервисе.
Порядок регистрации описан в [документации TipTop Pay](https://tiptoppay.kz/#connect).

## Возможности
	
* Одностадийная схема оплаты;  
* Двухстадийная схема оплаты;  
* Выбор языка виджета;  
* Выбор валюты виджета; 
* Выбор дизайна виджета;  
* Поддержка онлайн-касс (ФЗ-54);
* Выбор тегов предмета и способа расчета;  
* Отправка чеков по email;  
* Отправка чеков по SMS;  

# Установка

1. Скачайте папку с модулем и заархивируйте ее в архив `.zip`.  
2. В панели управления сайтом через меню перейдите в `Модули` - `Модули и сервисы`.  
3. Нажмите `Загрузить модуль`, выберете архив с модулем, дождитесь загрузки и установки, затем нажмите `Настройка`.  

# Настройка модуля

1. В списке модулей найдите **TipTop Pay** и нажмите `Установить` -> `Настроить`.  
2. Введите **Public ID** и **Пароль для API** (их можно найти в Личном кабинете TipTop Pay). Нажмите `Сохранить`.

### Настройка вебхуков

В [личном кабинете](https://merchant.tiptoppay.kz) TipTop Pay в настройках вашего сайта вставьте следующие URL для коректной работы модуля:

* **Check**  
`https://domain.kz/modules/tiptoppay/callback.php?callback_type=check`
* **Pay**  
`https://domain.kz/modules/tiptoppay/callback.php?callback_type=pay`
* **Fail**  
`https://domain.kz/modules/tiptoppay/callback.php?callback_type=fail`
* **Confirm**  
`https://domain.kz/modules/tiptoppay/callback.php?callback_type=confirm`
* **Refund**  
`https://domain.kz/modules/tiptoppay/callback.php?callback_type=refund`
* **Cancel**  
`https://domain.kz/modules/tiptoppay/callback.php?callback_type=cancel`

Где **domain.kz** - адрес сайта.

### Changelog

**1.0.0**
- Публикация модуля