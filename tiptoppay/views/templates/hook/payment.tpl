<div class="row">
	<div class="col-xs-12 col-md-6">
		<p class="payment_module">
			<a href="javascript:void(0);" onclick="pay();" title="{l s='Pay with TipTopPay' mod='tiptoppay'}" rel="nofollow" class="tiptoppay">
				{l s='Pay with TipTopPay' mod='tiptoppay'}
			</a>
		</p>
	</div>
</div>
<style>
	p.payment_module a.tiptoppay {
    	background: url({$path}views/img/logo_45x45.png) 23px 20px no-repeat;
	}
	p.payment_module a.tiptoppay:hover {
	    background-color: #f6f6f6;
	}
</style>
<script>
this.pay = function () {
    var widget = new tiptop.Widget();
    widget.{$payType}({ // options
            publicId: '{$publicId}',  //id из личного кабинета
            description: '{$description}', //назначение
            amount: {$totalPay}, //сумма
            currency: '{$currency}', //валюта
            invoiceId: {$cartId},
		    accountId: '{$accountId}',
		    skin: '{$skin}',
			data: {$additionalData}
        },
        function (options) { // success
            location.href = '{$redirectUrl}';
        });
};
</script>
